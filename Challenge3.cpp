#include<iostream>
#include<bits/stdc++.h>
#include<cstring>
#include<string>


using namespace std;

void cpalindrome_test(char* , int); // Checking and Reversed Word by Cstring Function

void spalindrome_test(string); //Checking and Reversed Word by String Function


//Use for output
void palindromescheck(char* word,int length,string strWord){
	string tempString = strWord;
	reverse(tempString.begin(),tempString.end());
	
	
	
	cpalindrome_test(word,length);
	cout <<endl;
	spalindrome_test(strWord);
	
}


//Main 
int main(){
	cout <<setw(48) << "Welcome to Palindrome Checker kub eiei"<<endl;
	cout << "=========================================================="<<endl;
	
    const int length = 100;
	char word[length];

	cout << "Enter the word with no space : ";
	cin >> word;
	
	string strWord = word;
	
	palindromescheck(word,length,strWord);
	
	
}


//// Checking and Reversed Word by Cstring
void cpalindrome_test(char* word,int length){
	char temp[length];
	strcpy(temp,word);
	strrev(temp);
	
	cout << "Your Reversed Word by Cstring is : " << temp<< endl;
	
	if(strcmp(temp,word)==0){
		cout << "This Is A Palindome.";
	}else{
		cout << "This Is Not A Palindrome.";
	}
	cout << endl << "--------------------------------"<<endl <<"Checking By Cstring"<<endl << "--------------------------------";
}



//Checking and Reversed Word by String

void spalindrome_test(string strWord){
	string strRev = strWord;
	string reversed_word(strRev.rbegin(),strRev.rend());
	cout << "Your Reversed Word By String is : " << reversed_word <<endl;
	if(reversed_word==strWord){
		cout << "This Is A Palindrome";
	}
	else{
		cout << "This Is Not A Palindrome";
	}
	cout << endl << "--------------------------------"<<endl <<"Checking By String" << endl << "--------------------------------";
}





